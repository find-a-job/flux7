key_name = {
  us-east-1 = "flux7-us-east-1"
}

ami = {
  us-east-1 = "ami-0de5dbbba6b284d6f"
}

count = 1

tags = {
  Name           = "Flux7 EC2"
  MachineName = "ue1-adm-osl-404"
  Terraform      = "true"
}

#volume_tags = {
#  Name           = "Flux7 EC2"
#  "Machine Name" = "ue1-adm-osl-404"
#  Terraform      = "true"
#}
