# Take home assignment

* I've had the thing for a long time, and could have been working on it ; not sure how I forgot it :|

* from Heather Nadeau

    ```text
    Hello Tim, 

    Thank you for speaking with the team. As a next step we would like to give you two take home assignments. Please let us know if you have any questions regarding these. 

    You will typically have 2-3 days to complete these assignments and we will then schedule a review call to walk us through the solutions. 
 
    Assignment:

        * Ansible (2.5+) has recently released a module for terraform. Please create an Ansible playbook to deploy terraform projects including deciding appropriate parameters to provide a good balance between usability and customizability. This playbook is intended to be run within a Jenkins pipeline.

    Think about it as a generic playbook to deploy any terraform project. The idea is to have various capabilities like

    * Ability to set backend configuration during runtime
    * Ability to pass terraform variables through ansible vars
    * Have a way to create:
        * Only terraform plan and not execute it (for example, this can be used to create a manual approval stage)
        * Create the plan and execute it
        * Just execute a plan that has been created earlier

    If you think the new module lacks these capabilities. please mention them in detail.

    Thank You,
    Heather Nadeau

    Please submit here:
    <https://app2.greenhouse.io/tests/570e6c473ada709eac048ffa5016613e>
    ```
